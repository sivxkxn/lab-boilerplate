const funcModule = require('./app');
const path = './mock_for_L3.js';
let userMock = require(path).randomUserMock;
let additionalUsers = require(path).additionalUsers;
//the first task: merge
let merged = funcModule.userMerge(userMock, additionalUsers);
console.log(merged);
//the second task: validation
merged.map((x) => {
    console.log(funcModule.userValidation(x));
});
//the third task: filter
console.log(funcModule.userFilter(merged, 'country', 'Norway'));
//the fourth task:sort
console.log(funcModule.userSort(merged, 'age', 'desc'))
//the fifth task: search
console.log(funcModule.userSearch(merged, 'age', 28));
//the sixth task: persentage
console.log(funcModule.userPrecentage(merged, (x) => {
    if (x.age < 28) return x;
}))