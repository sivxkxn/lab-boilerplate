// const testModules = require('./test-module');
// require('../css/app.css');
// require('../scss/style.scss');

const fs = require('fs');
const countriesJSON = fs.readFileSync('./../../codes.json');
const listOfCountries = JSON.parse(countriesJSON).country;

function userMerge(arr, additionalArr) {
    let counter = 0;
    let formattedArr = arr.map(obj => {
        counter++;
        let newObj = new Object({
            gender: obj.gender || null,
            title: obj.name.title || null,
            full_name: obj.name.first + ' ' + obj.name.last || null,
            city: obj.location.city || null,
            state: obj.location.state || null,
            country: obj.location.country || null,
            postcode: obj.location.postcode || null,
            coordinates: obj.location.coordinates || null,
            timezon: obj.location.timezon || null,
            email: obj.email || null,
            b_day: obj.dob.date || null,
            age: obj.dob.age || null,
            phone: obj.phone || null,
            picture_large: obj.picture.large || null,
            picture_thumbnail: obj.picture.thumbnail || null,
            id: counter,
            favorite: false,
            course: '',
            bg_color: '#fffff',
        });
        return newObj;
    });
    let uniqueArr = formattedArr.filter(x => {
        if (undefined === additionalArr.find(y => y.full_name === x.full_name))
            return x;
    });
    const result = uniqueArr.concat(additionalArr);
    return result;
}

function userValidation(obj) {

    let stringFieldsValudation = function (obj, field) {
        if (typeof (obj[field]) === 'string' || obj[field] instanceof String) {
            if (obj[field][0].match(/^[A-Z]/g)) return true;
            else return false;
        }
        return false;
    }

    let ageValidation = function (obj, field) {
        if (!isNaN(obj[field])) return true;
        return false;
    }

    let phoneValidation = function (obj, field) {
        let phone = '';
        if (typeof (obj[field]) === 'string' || obj[field] instanceof String) {
            phone = obj[field];
            let countryObj = listOfCountries.find(x => x['eng'] === obj['country']);
            if (countryObj === undefined)
                return false
            phone = phone.split('').filter(x => {
                if (x !== '(' && x !== ')' && x !== '-' && x !== '+')
                    return x;
            }).join('');
            if (phone.includes(Number(countryObj.code)) && phone.length === Number(countryObj.length))
                return true;
            return false;
        }
    }

    let mailValidation = function (obj, field) {
        if (typeof (obj[field]) === 'string' || obj[field] instanceof String) {
            const reg = /^[a-zA-Z0-9.!#$%&'*+/=?^_`{|}~-]+@[a-zA-Z0-9-]+(?:\.[a-zA-Z0-9-]+)*$/g;
            if (obj[field].match(reg)) return true;
        }
        return false;
    }

    let results = [];
    for (let field in obj) {
        if (field === 'full_name' || field === 'gender' || field === 'state' || field === 'city' || field === 'note')
            results.push(stringFieldsValudation(obj, field));
        if (field === 'age') results.push(ageValidation(obj, field));
        if (field === 'phone') results.push(phoneValidation(obj, field));
        if (field === 'email') results.push(mailValidation(obj, field))
    }
    if (results.includes(false)) return false;
    else return true;
}

function userFilter(arr, field, value) {
    return arr.filter(x => x[field] === value)
}

function userSort(arr, field, order) {
    let sortedArr = [];
    if (field === 'full_name' || field === 'country') {
        sortedArr = arr.sort((a, b) => a[field] > b[field] ? 1 : -1);
        if (order === 'asc') return sortedArr;
        else if (order === 'desc') return sortedArr.reverse();
    }
    else if (field === 'age') {
        sortedArr = arr.sort((a, b) => Number(a[field]) > Number(b[field]) ? 1 : -1);
        if (order === 'asc') return sortedArr;
        else if (order === 'desc') return sortedArr.reverse();
    }
    else if (field === 'b_day') {
        sortedArr = arr.sort((a, b) => new Date(a[field]) > new Date(b[field]) ? 1 : -1)
        if (order === 'asc') return sortedArr;
        else if (order === 'desc') return sortedArr.reverse();
    }
}


function userSearch(arr, field, value) {
    return arr.find(x => x[field] === value);
}

function userPrecentage(arr, func) {
    if (arr.length === 0) return 0;
    const filteredArr = arr.filter(x => func(x));
    return (filteredArr.length / arr.length) * 100;

}

module.exports = { userMerge, userValidation, userFilter, userSort, userSearch, userPrecentage }
/** ******** Your code here! *********** */
// eslint-disable-next-line no-console
// console.log(testModules.hello);